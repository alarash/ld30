﻿using UnityEngine;
using System.Collections;

public class RepairScript : MonoBehaviour {

	public void OnUse(GameObject target){
		Health health = target.GetComponent<Health>();
		if(health != null){
			health.health = health.maxHealth;
		}
	}
}
