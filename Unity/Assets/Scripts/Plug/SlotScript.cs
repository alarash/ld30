﻿using UnityEngine;
using System.Collections;

public class SlotScript : MonoBehaviour {
	ObjectSlotScript objectSlot;
	public PlugScript plug;
	
	public MachineScript[] machines;
	
	void Start(){
		objectSlot = GetComponent<ObjectSlotScript>();
		
	}
	
	/*void Update(){
		
	}*/
	
	public void OnAttachObject(){
		plug = objectSlot.attachedObject.GetComponent<PlugScript>();
		if(plug == null) return;
		
		foreach(MachineScript machine in machines){
			if(machine == null) continue;
			machine.SetPlug(plug);
		}
		
	}
	
	public void OnDetachObject(){
		foreach(MachineScript machine in machines){
			if(machine == null) continue;
			machine.SetPlug(null);
		}
	}
}
