﻿using UnityEngine;
using System.Collections;

public class ObjectScript : MonoBehaviour {
	public AirShip parentShip;
	public ObjectSlotScript parentSlot;
	SpringJoint spring;
	
	public bool detroyOnUse = false;
	
	public AudioClip grabClip;
	
	public GameObject explosion;

	/*void Start () {
		spring = GetComponent<SpringJoint>();
	}*/
	
	public void AttachSpring(AirShip parent){
		if(parentSlot != null){
			//DetachSlot();
			parentSlot.DetachObject();
			parentSlot = null;
		}
			
		if(spring == null)
			spring = GetComponent<SpringJoint>();
			
		parentShip = parent;
		transform.position = parent.anchorPoint.position;
		transform.rotation = parent.anchorPoint.rotation;
		print(spring);
		print(parent.GetComponent<Rigidbody>());
		spring.connectedBody = parent.GetComponent<Rigidbody>();
		spring.spring = 10f;
		
		GetComponent<AudioSource>().clip = grabClip;
		GetComponent<AudioSource>().Play();
		
		SendMessage("OnAttachSpring", SendMessageOptions.DontRequireReceiver);
		
		
	}
	
	public void DetachSpring(){
		parentShip = null;
		spring.connectedBody = null;
		spring.spring = 0f;
		
		SendMessage("OnDetachSpring", SendMessageOptions.DontRequireReceiver);
	}
	
	public void AttachSlot(ObjectSlotScript parent){
		if(parentShip != null){
			parentShip.DetachObject();
			parentShip = null;
		}
			
		parentSlot = parent;
		
		GetComponent<Rigidbody>().isKinematic = true;
			
		transform.position = parentSlot.anchorPoint.position;
		transform.rotation = parentSlot.anchorPoint.rotation;
		
		SendMessage("OnAttachSlot", SendMessageOptions.DontRequireReceiver);
		
	}
	
	public void DetachSlot(){
		parentSlot = null;
		GetComponent<Rigidbody>().isKinematic = false;
		
		SendMessage("OnDetachSlot", SendMessageOptions.DontRequireReceiver);
	}

	public void Use (GameObject target)
	{
		SendMessage("OnUse", target, SendMessageOptions.DontRequireReceiver);
		
		if(detroyOnUse)
			Destroy(gameObject);
	}
	
	public void OnKilled(){
		if(explosion != null){
			Instantiate(explosion, transform.position, transform.rotation);
		}
		Destroy(gameObject);
	}
}
