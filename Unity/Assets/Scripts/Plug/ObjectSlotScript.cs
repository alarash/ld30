﻿using UnityEngine;
using System.Collections;

public class ObjectSlotScript : MonoBehaviour {
	public ObjectScript attachedObject;
	public Transform anchorPoint;
	
	void OnTriggerEnter(Collider other) {
		if(attachedObject != null) return;
		ObjectScript obj = other.GetComponent<ObjectScript>();
		if(obj == null) return;
		
		PlugScript plug = other.GetComponent<PlugScript>();
		if(plug == null) return;
		
		AttachObject(obj);
	}
	
	public void AttachObject(ObjectScript obj){
		attachedObject = obj;
		obj.AttachSlot(this);
		
		SendMessage("OnAttachObject", SendMessageOptions.DontRequireReceiver);
	}
	
	public void DetachObject(){
		if(attachedObject == null) return;
		attachedObject.DetachSlot();
		attachedObject = null;
		
		SendMessage("OnDetachObject", SendMessageOptions.DontRequireReceiver);
	}
}
