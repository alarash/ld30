﻿using UnityEngine;
using System.Collections;

public enum PlugState {
	OFF,
	ON,
	DAMAGED
}

public class PlugScript : MonoBehaviour {
	ObjectScript obj;
	Connection conn;
	
	public PlugState state;
	
	public GameObject offGameObject;
	public GameObject onGameObject;
	public GameObject damagedGameObject;

	// Use this for initialization
	void Start () {
		obj = GetComponent<ObjectScript>();
		conn = GetComponent<Connection>();
		conn.enabled = false;
		GetComponent<LineRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		offGameObject.SetActive(state == PlugState.OFF);
		onGameObject.SetActive(state == PlugState.ON);
		damagedGameObject.SetActive(state == PlugState.DAMAGED);
		
		conn.enabled = state == PlugState.ON;
	}
	
	public void OnAttachSpring(){
		//audio.Play();
	}
	
	public void OnAttachSlot(){
		print("OnAttachSlot" + obj.parentSlot.ToString());
		
		if(state == PlugState.OFF)
			state = PlugState.ON;
			
		GetComponent<AudioSource>().Play();
		conn.enabled = true;
	}
	
	public void OnDetachSlot(){
		if(state == PlugState.ON)
			state = PlugState.OFF;
		conn.enabled = false;
	}
	
	public void SetState(PlugState state){
		this.state = state;
		conn.enabled = state == PlugState.ON;
	}
}
