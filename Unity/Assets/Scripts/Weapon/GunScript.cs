﻿using UnityEngine;
using System.Collections;

public class GunScript : MonoBehaviour {

	public GameObject missilePrefab;
	
	float timer = 0f;
	public float cooldownTime = 1f;
	
	public Vector3 impulseVector;
	
	void Update(){
		if(timer > 0){
			timer -= Time.deltaTime;
			if(timer < 0)
				timer = 0;
		}
	}
	
	public bool CanFire(){
		return timer == 0;
	}

	public void Fire(){
		if(!CanFire()) return;
		timer = cooldownTime;
		GameObject instance = Instantiate(missilePrefab, transform.position, transform.rotation) as GameObject;
		
		instance.GetComponent<Rigidbody>().AddRelativeForce(impulseVector);
	}
}
