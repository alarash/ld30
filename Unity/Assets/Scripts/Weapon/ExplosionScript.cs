﻿using UnityEngine;
using System.Collections;

public class ExplosionScript : MonoBehaviour {

	public int damage = 1;
	public float force = 30f;
	public float radius = 3f;

	// Use this for initialization
	void Start () {
		Destroy(gameObject, 2f);
		
		Collider[] cols = Physics.OverlapSphere(transform.position, radius);
		foreach(Collider c in cols){
			c.SendMessage("OnDamage", damage, SendMessageOptions.DontRequireReceiver);
			
			Rigidbody rb = c.GetComponent<Rigidbody>();
			if(rb != null){
				rb.AddExplosionForce(force, transform.position, radius);
			}
		}
	}
	

}
