﻿using UnityEngine;
using System.Collections;

public class MissileScript : MonoBehaviour {

	public GameObject explosionPrefab;
	
	public float destroyTimer = 5f;
	float timer = 0f;
	
	public int damage = 1;
	
	void Start(){
		Destroy(gameObject, destroyTimer);
	}
	
	void Update(){
		timer += Time.deltaTime;
	}
	
	void OnTriggerEnter(Collider other){
		if(timer < 0.02f) return;
		Explosion();
		
		other.SendMessage("OnDamage", damage, SendMessageOptions.DontRequireReceiver);
		
		/*Health health = other.GetComponent<Health>();
		if(health != null){
			health.
		}*/
	}

	void Explosion () {
		if(explosionPrefab != null){
			Instantiate(explosionPrefab, transform.position, transform.rotation);
		}
		
		Destroy(gameObject);
	}

}
