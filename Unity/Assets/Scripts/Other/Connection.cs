﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Connection : MonoBehaviour {

	LineRenderer line;

	public GameObject input;
	public GameObject output;

	// Use this for initialization
	void Start () {
		line = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if(line == null)
			Start ();
		
		if(input == null || output == null){
			line.enabled = false;
			return;
		}
		else {
			line.enabled = true;
		}
		
		line.SetPosition(0, input.transform.position);
		line.SetPosition(1, output.transform.position);
	}
	
	void OnEnable() {
		if(line == null) return;
		line.enabled = true;
	}
	
	void OnDisable(){
		if(line == null) return;
		line.enabled = false;
	}
}
