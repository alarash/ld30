﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public static GameManager Instance;
	
	SlotScript[] slots;
	int connectedSlots = 0;
	
	public Text islandsTxt;
	public GameObject gameOverPanel;
	public Text gameOverTitle;
	public Text gameOverTxt;
	
	public PlayerScript player;
	
	void Awake(){
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		slots = GameObject.FindObjectsOfType<SlotScript>();
		
		Time.timeScale = 1f;
	}
	
	// Update is called once per frame
	void Update () {
		connectedSlots = 0;
		foreach(SlotScript slot in slots){
			if(slot.plug != null && slot.plug.state != PlugState.OFF)
				connectedSlots++;
		}
		
		if(connectedSlots == slots.Length)
			Win ();
		
		islandsTxt.text = "Connected Islands: "+connectedSlots.ToString()+"/"+slots.Length.ToString();
	}
	
	public void Restart(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
	
	public void GameOver(string msg){
		//yield return new WaitForSeconds(1f);
		
		Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;
		
		gameOverPanel.SetActive(true);
		gameOverTxt.text = msg;
		
		Time.timeScale = 0f;
	}
	
	public void Win(){
		//yield return new WaitForSeconds(1f);
		
		gameOverPanel.SetActive(true);
		gameOverTitle.text = "You Win";
		gameOverTxt.text = "You managed to connect all the island and restore the power, Good job !";
		
		Time.timeScale = 0f;
	}
}
