﻿using UnityEngine;
using System.Collections;

public class HideText : MonoBehaviour {

	public GameObject canvas;
	
	public void OnInteract(AirShip ship){
		//if(ship.attachedObject != null) return;
		canvas.SetActive(false);
	}
	
	public void OnAttachObject(){
		canvas.SetActive(false);
	}
}
