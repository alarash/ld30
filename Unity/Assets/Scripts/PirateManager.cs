﻿using UnityEngine;
using System.Collections;

public class PirateManager : MonoBehaviour {

	public GameObject piratePrefab;

	public float timer = 0f;
	public float nextTimer = 20f;
	
	public float spawnDistance = 400f;
	
	void Update () {
		if(timer <= 0f){
			timer = nextTimer;
			Spawn();
		}
		else {
			timer -= Time.deltaTime;
		}
		
	}
	
	void Spawn(){
		Vector3 pos = Random.onUnitSphere * spawnDistance;
		/*GameObject instance = */Instantiate(piratePrefab, pos, Quaternion.identity)/* as GameObject*/;
	}
}
