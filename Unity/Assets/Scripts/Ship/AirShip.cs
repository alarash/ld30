﻿using UnityEngine;
using System.Collections;

public class AirShip : MonoBehaviour {

	public float speed = 10f;
	public float turnSpeed = 1f;
	
	public Engine[] engines;
	public GunScript[] guns;
	
	public float thrust = 0;
	public float rotHorizontal = 0;
	public float rotVertical = 0;
	
	public ObjectScript attachedObject;
	public Transform anchorPoint;
	
	public GameObject explosion;
	
	void FixedUpdate () {
		if(thrust > 0){
			GetComponent<Rigidbody>().AddRelativeForce (Vector3.forward *speed * thrust);
		}
		
		foreach(Engine e in engines){
			e.SetThrust(thrust);
		}
		
		GetComponent<Rigidbody>().AddRelativeTorque (rotVertical * turnSpeed, 0, 0);
		GetComponent<Rigidbody>().AddRelativeTorque (0, rotHorizontal * turnSpeed, 0);
		
		Quaternion flatRot = Quaternion.LookRotation(transform.forward - Vector3.up * transform.forward.y, Vector3.up);
		transform.rotation = Quaternion.Slerp(transform.rotation, flatRot, Time.deltaTime);
	}
	
	public void AttachObject(ObjectScript obj){
		print("AttachObject"+obj.ToString());
		attachedObject = obj;
		obj.AttachSpring(this);
	}
	
	public void DetachObject(){
		if(attachedObject == null) return;
		attachedObject.DetachSpring();
		attachedObject = null;
	}

	public void UseAttached (GameObject target)
	{
		if(attachedObject == null) return;
		
		attachedObject.Use(target);
		
		/*if(attachedObject.Use(target)){
			DetachObject();
		}*/
		
	}
	
	public void OnKilled(){
		Instantiate(explosion, transform.position, transform.rotation);
		Destroy(gameObject);
	}
}
