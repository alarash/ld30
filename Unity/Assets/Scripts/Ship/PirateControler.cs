﻿using UnityEngine;
using System.Collections;

public class PirateControler : MonoBehaviour {

	AirShip ship;
	
	GameObject targetGO;
	

	float avoidTimer = 0;


	void Start () {
		ship = GetComponent<AirShip>();
	}
	

	void FixedUpdate () {
		GetTarget();
		
		if(CheckAvoid()){
			Avoid();
		}
		else {
			GotoTargetPosition();
			
			if(CheckTargetInRange()){
				Fire();
			}
		}
		
		
	}
	
	void GetTarget(){
		if(targetGO != null){
			Health health = targetGO.GetComponent<Health>();
			if(health != null && health.health > 0)
				return;
		}
		
		LightHouseScript[] lighthouses = GameObject.FindObjectsOfType<LightHouseScript>();
		foreach(LightHouseScript lighthouse in lighthouses){
			if(lighthouse.isON){
				targetGO = lighthouse.gameObject;
				return;
			}
		}
		GameObject[] targets = GameObject.FindGameObjectsWithTag("Player");
		GameObject target = targets[Random.Range(0, targets.Length)];
		
		targetGO = target;
		

	}
	
	bool CheckAvoid(){
		/*return false;*/
		
		
		if(avoidTimer > 0){
			avoidTimer -= Time.deltaTime;
			if(avoidTimer < 0)
				avoidTimer = 0;
				
			return true;
		}
		
		RaycastHit hit;
		if(avoidTimer == 0 && Physics.Raycast(new Ray(transform.position, transform.forward), out hit, 3f)){
			avoidTimer = 2f;
			return true;	
		}
		
		return false;
	}
	
	void Avoid(){
		//rigidbody.AddRelativeTorque(-15f, 0, 0);
		
		ship.thrust = 0.3f;
		ship.rotVertical = -1f;
		ship.rotHorizontal = -0.1f;
	}
	
	void GotoTargetPosition(){
		float distance = Vector3.Distance(targetGO.transform.position, transform.position);
		if(distance > 10f)
			ship.thrust = 0.6f;
		else 
			ship.thrust = 0f;
			
		Quaternion q = Quaternion.LookRotation(targetGO.transform.position - transform.position, Vector3.up);
		float rotH = q.eulerAngles.y;
		
		rotH -= transform.rotation.eulerAngles.y;
		if(rotH > 180f) rotH -= 360f;
		else if(rotH < -180f) rotH += 360f;
		
		float rotV = q.eulerAngles.x;
		
		rotV -= transform.rotation.eulerAngles.x;
		if(rotV > 180f) rotV -= 360f;
		else if(rotV < -180f) rotV += 360f;
		
		
		//ship.thrust = 0.6f;
		ship.rotHorizontal = rotH;
		ship.rotVertical = rotV;
	}

	bool CheckTargetInRange ()
	{
		float distance = Vector3.Distance(targetGO.transform.position, transform.position);
		return distance < 30f;
	}

	void Fire ()
	{
		foreach(GunScript gun in ship.guns)
			gun.Fire();
	}
}
