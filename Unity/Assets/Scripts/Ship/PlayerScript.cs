﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	AirShip ship;
	
	public Texture2D cursorDefault;
	public Texture2D cursorHighlight;
	Texture2D cursor;
	
	public ObjInfoScript currentObjInfo;
	GameObject current;

	void Start () {
		ship = GetComponent<AirShip>();
		
		cursor = cursorDefault;
		
		//Cursor.SetCursor(cursorDefault, new Vector2(32f, 32f), CursorMode.Auto);
	}
	
	void Update() {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		current = null;
		float distance = -1;
		
		if (Physics.Raycast(ray, out hit, 100f, 1 << 8)){
			//print (hit.collider.gameObject);
			
			current = hit.collider.gameObject;
			distance = hit.distance;
			
			if(distance < 15){
				//Cursor.SetCursor(cursorHighlight, new Vector2(16, 16), CursorMode.Auto);
				cursor = cursorHighlight;
			}
		}
		else {
			//Cursor.SetCursor(cursorDefault, new Vector2(16, 16), CursorMode.Auto);
			cursor = cursorDefault;
		}
		
		if(Input.GetMouseButtonDown(0) && current != null && distance < 15){
			ship.UseAttached(current);
		}
		
		if(Input.GetMouseButtonDown(1)){
			print("Right Click");
			if(ship.attachedObject != null){
				print("Detach");
				ship.DetachObject();
			}
			else if(current != null && distance < 15){
				ObjectScript obj = current.GetComponent<ObjectScript>();
				print("Attach "+obj.ToString());
				if(obj != null)
					ship.AttachObject(obj);
			}
		}
		
		if(Input.GetButtonDown("Interact") && current != null && distance < 15){
			MachineScript machine = current.GetComponent<MachineScript>();
			if(machine != null)
				machine.Interact(ship);
		}
		
		currentObjInfo.obj = current;
	}
	
	void FixedUpdate(){
		//float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");
		
		Vector3 relMouse = Input.mousePosition - new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0);
		relMouse *= 0.01f;
		
		ship.thrust = vertical;
		ship.rotVertical = -relMouse.y;
		ship.rotHorizontal = relMouse.x;
	}
	
	void OnGUI(){
		Cursor.visible = false;
		GUI.DrawTexture(new Rect(
			Input.mousePosition.x - 16, 
			Screen.height - Input.mousePosition.y - 16, 
			32, 32),
			cursor
		);
	}
	
	public void OnKilled(){
		//GameManager.Instance.StartCoroutine("GameOver", "Your airship was destroyed");
		GameManager.Instance.GameOver("Your airship was destroyed");
	}
	
}
