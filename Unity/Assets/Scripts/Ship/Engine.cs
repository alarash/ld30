﻿using UnityEngine;
using System.Collections;

public class Engine : MonoBehaviour {

	public ParticleSystem[] engineParticles;
	
	public GameObject mesh;
	public Color burnColor;
	
	Color currentColor = Color.black;
	
	public void SetThrust(float thrust){
	
		foreach(ParticleSystem ps in engineParticles){
			//ps.enableEmission = thrust > 0;
            ParticleSystem.EmissionModule em = ps.emission;
            em.enabled = thrust > 0;
		}
		
		Color targetColor;
		if(thrust > 0){
			if(!GetComponent<AudioSource>().isPlaying)
				GetComponent<AudioSource>().Play();
			
			
			targetColor = burnColor;
		}
		else {
			GetComponent<AudioSource>().Stop();
			
			targetColor =  Color.black;
		}
		
		currentColor = Color.Lerp(currentColor, targetColor, Time.deltaTime * 5f);
		mesh.GetComponent<Renderer>().materials[1].SetColor("_Emission", currentColor);
	}
}
