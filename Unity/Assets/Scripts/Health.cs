﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public int maxHealth = 10;
	public int health = 10;
	
	public void OnDamage(int amount){
		if(health < 0) return;
		
		health -= amount;
		if(health <= 0){
			health = 0;
			SendMessage("OnKilled", SendMessageOptions.DontRequireReceiver);
		}
	}
}
