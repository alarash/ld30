﻿using UnityEngine;
using System.Collections;

public class CloudScript : MonoBehaviour {

	public int maxParticles = 500;
	
	public Color topColor;
	public Color bottomColor;

	ParticleSystem ps;
	void Start () {
		ps = GetComponent<ParticleSystem>();
		ParticleSystem.Particle[] particles = new ParticleSystem.Particle[maxParticles];

		ps.Emit(maxParticles);
		int count = ps.GetParticles(particles);

		for(int i = 0; i < count; i++){
			particles[i].startColor = Color.Lerp(bottomColor, topColor, particles[i].position.y);
		}
		
		ps.SetParticles(particles, maxParticles);
	}
	
	void Update () {
		
	}
	
	void OnDrawGizmos() {
		Gizmos.color = Color.white;
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.DrawWireSphere(Vector3.zero, 1);
		
	}
}
