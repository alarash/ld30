﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

	public Transform target;
	public Vector3 offset;
	public Vector3 offsetCam;
	
	public float speed = 1;
	
	// Update is called once per frame
	void FixedUpdate () {
		if(target == null) return;
		
		transform.position = Vector3.Lerp(transform.position, target.TransformPoint(offset), Time.deltaTime * speed);
		transform.LookAt(target.position + offsetCam, target.up);
	}
}
