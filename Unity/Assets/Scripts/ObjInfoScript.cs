﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObjInfoScript : MonoBehaviour {

	public GameObject obj = null;
	
	public GameObject panel;
	
	public Text objNameText;
	public Slider healthSlider;
	public Text healthText;
	public Text machineText;
	
	Health health;
	MachineScript machine;
	
	// Update is called once per frame
	void Update () {
		panel.SetActive(obj != null);
		if(machineText != null)
			machineText.enabled = obj != null;
			
		if(obj == null){
			health = null;
			return;
		}
		
		if(health == null){
			health = obj.GetComponent<Health>();
			machine = obj.GetComponent<MachineScript>();
		}
		
		
		objNameText.text = obj.name;
		
		healthSlider.maxValue = health.maxHealth;
		healthSlider.value = health.health;
		
		healthText.text = health.health.ToString()+" / "+health.maxHealth.ToString();
		
		if(machineText != null){
			machineText.enabled = machine != null;
			if(machine != null){
				machineText.text = machine.state.ToString();
			}
		}
	}
}
