﻿using UnityEngine;
using System.Collections;

public class PowerSource : MonoBehaviour {

	public GameObject laserSource;
	public GameObject prefab;
	
	//public GameObject explosion;
	

	public void OnInteract(AirShip ship){
		if(ship.attachedObject != null) return;
		GameObject instance = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
		instance.name = prefab.name;
		ObjectScript obj = instance.GetComponent<ObjectScript>();
		obj.GetComponent<Connection>().output = laserSource;
		
		ship.AttachObject(obj);
	}
	
	public PlugScript[] GetPlugs(){
		return GameObject.FindObjectsOfType<PlugScript>();
	}
	
	public void OnActivate(){
		foreach(PlugScript plug in GetPlugs()){
			//plug.state = PlugState.ON;
			plug.SetState(PlugState.ON);
		}
	}
	
	public void OnDeactivate(){
		foreach(PlugScript plug in GetPlugs()){
			//plug.state = PlugState.OFF;
			plug.SetState(PlugState.OFF);
		}
	}
	
	public void OnKilled(){
		//Instantiate(explosion, transform.position, transform.rotation);
		//GameManager.Instance.StartCoroutine("GameOver", "You lost the Power Source");
		//Destroy(gameObject);
		GameManager.Instance.GameOver("You lost the Power Source");
	}
}
