﻿using UnityEngine;
using System.Collections;

public class LightHouseScript : MonoBehaviour {
	MachineScript machine;
	
	
	public RotY rot;
	public Light rotlight;
	
	public bool isON = false;

	// Use this for initialization
	void Start () {
		machine = GetComponent<MachineScript>();
	}
	
	// Update is called once per frame
	void Update () {
		if(machine.state == MachineState.ON)
			OnActivate();
		else
			OnDeactivate();
	}
	
	public void OnActivate(){
		rot.enabled = true;
		rotlight.enabled = true;
		isON = true;
	}
	
	public void OnDeactivate(){
		rot.enabled = false;
		rotlight.enabled = false;
		isON = false;
	}
}
