﻿using UnityEngine;
using System.Collections;

public class TurretScript : MonoBehaviour {
	MachineScript machine;
	
	public GameObject rotY;
	public GameObject rotX;
	
	public GunScript gun;
	
	public GameObject target;
	
	public float distanceRatio = 10f;

	void Start () {
		machine = GetComponent<MachineScript>();
	}
	
	void Update () {
		if(machine.state != MachineState.ON)
			return;
			
		GetTarget();
		OrientToTarget();
		if(AlignToTarget())
			Shoot();
	}

	void GetTarget ()
	{
		//if(target != null) return;
		
		PirateControler[] pirates = GameObject.FindObjectsOfType<PirateControler>();
		float dist = 10000f;
		PirateControler nearest = null;
		foreach(PirateControler pirate in pirates){
			float distance = Vector3.Distance(transform.position, pirate.transform.position);
			if(distance < dist){
				nearest = pirate;
				dist = distance;
			}
		}
		if(nearest != null)
			target = nearest.gameObject;
	}

	void OrientToTarget ()
	{
		if(target == null) return;
		
		float distance = Vector3.Distance(rotX.transform.position, target.transform.position);
		
		Vector3 direction = (
			target.transform.position - rotX.transform.position + 
			Vector3.up * distance / distanceRatio).normalized;
		
		
		Quaternion lookRotation = Quaternion.LookRotation(direction);


		Quaternion qx = Quaternion.Lerp(
			rotX.transform.rotation,
			lookRotation, 
			Time.deltaTime
		);
		
		rotX.transform.rotation = qx;
		rotY.transform.rotation = Quaternion.Euler(0, qx.eulerAngles.y, 0);	
	}

	bool AlignToTarget ()
	{
		return true;
	}

	void Shoot ()
	{
		gun.Fire();
	}
}
