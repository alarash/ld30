﻿using UnityEngine;
using System.Collections;

public enum MachineState {
	OFF,
	ON
}

public class MachineScript : MonoBehaviour {
	public MachineState state;
	
	public bool needPlug = true;
	PlugScript plug;
	
	public GameObject explosion;

	public void SetPlug (PlugScript plug)
	{
		this.plug = plug; 
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(needPlug){
			if(plug == null){
				if(state == MachineState.ON){
					Deactivate();
				}
			}
			else {
				if(plug.state != PlugState.OFF && state == MachineState.OFF){
					Activate();
				}
				else if(plug.state == PlugState.OFF && state == MachineState.ON){
					Deactivate();
				}
			}
		}
		else {
		
		}
	}
	
	void Deactivate(){
		state = MachineState.OFF;
		SendMessage("OnDeactivate", SendMessageOptions.DontRequireReceiver);
	}
	
	void Activate(){
		state = MachineState.ON;
		SendMessage("OnActivate", SendMessageOptions.DontRequireReceiver);
	}
	
	void OnKilled(){
		Instantiate(explosion, transform.position, transform.rotation);
		Destroy(gameObject);
		
		//Deactivate();
	}

	public void Interact (AirShip ship)
	{
		SendMessage("OnInteract", ship, SendMessageOptions.DontRequireReceiver);
	}
}
