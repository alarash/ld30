﻿using UnityEngine;
using System.Collections;

public class RepairShopScript : MonoBehaviour {
	MachineScript machine;

	public GameObject item;
	
	void Start(){
		machine = GetComponent<MachineScript>();
	}
	
	public void OnInteract(AirShip ship){
		if(machine.state != MachineState.ON) return;
		
		Health health = ship.GetComponent<Health>();
		if(health.health < health.maxHealth)
			health.health = health.maxHealth;			
		
		if(ship.attachedObject != null) return;
		GameObject instance = Instantiate(item, transform.position, transform.rotation) as GameObject;
		instance.name = item.name;
		ObjectScript obj = instance.GetComponent<ObjectScript>();
		ship.AttachObject(obj);
	}
}
